import { Component, OnInit } from '@angular/core';

import { Product } from './entities/product.entity';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{

  product: Product;
  age: number;
  fullName: string;
  status: boolean;
  email: any;

  ngOnInit(): void {
      this.age = 26;
      this.fullName = 'Jayaprakash';
      this.status = true;
      this.email = 'jayaprakash@gmail.com';

      this.product = {
        id: 'P01',
        name: 'Yonex Muscle Power 29 Lite',
        photo: 'yonex_muscle_power_29_lite.jpg',
        price: 2000,
        quantity: 10
      };
  }

}
